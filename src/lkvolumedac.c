

#include <stdio.h>
#include <termios.h>
#include <unistd.h>


#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <dirent.h>
#include <time.h>
#if defined(__linux__) //linux
#define MYOS 1
#elif defined(_WIN32)
#define MYOS 2
#elif defined(_WIN64)
#define MYOS 3
#elif defined(__unix__) 
#define MYOS 4  // freebsd
#define PATH_MAX 2500
#else
#define MYOS 0
#endif
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h> 
#include <time.h>

int main()
{
        printf( " HELP: + or - to change the volume DAC\n" ); 

        struct termios ot;
        if(tcgetattr(STDIN_FILENO, &ot) == -1) perror(")-");
        struct termios t = ot;
        t.c_lflag &= ~(ECHO | ICANON);
        t.c_cc[VMIN] = 1;
        t.c_cc[VTIME] = 0;
        if( tcsetattr( STDIN_FILENO, TCSANOW, &t) == -1) perror(")-");
        int a;
        while(a = getchar(), a != 'q') 
	{
                fprintf( stderr, "you pressed '%c'\n", a);

	        if ( a ==  '+' )  
                  system( "  amixer -c 0 sset DAC 2+ " );
	        else if ( a ==  '-' )  
                  system( "  amixer -c 0 sset DAC 2- " );

	        else if ( a == 'u' )  
                  system( "  amixer -c 0 sset DAC 5+ " );
	        else if ( a == 'd' )  
                  system( "  amixer -c 0 sset DAC 5- " );  

	        else if ( a == 'j' )  
                  system( "  amixer -c 0 sset DAC 2- " );
	        else if ( a == 'k' )  
                  system( "  amixer -c 0 sset DAC 2+ " );  


        }
        if(tcsetattr(STDIN_FILENO, TCSANOW, &ot) == -1) perror(")-");
}



/*
  system( "  amixer -c 0 sset DAC 2- " );
  printf( "  amixer -c 0 sset DAC 2+ " );
  system( "  amixer -c 0 sset DAC 2+ " );
*/

